# Documentação Kanui Apps - Internal-Apps Docker 

1. Instruções Iniciais
2. Arquitetura 
3. Problemas

### 1. Instruções Iniciais

Instale o Docker! Caso precise de ajuda, acesse [esse link](https://www.digitalocean.com/community/tutorials/como-instalar-e-usar-o-docker-no-ubuntu-18-04-pt), certifique-se que seu usuário consiga rodar os comandos docker sem sudo.

1. Faça o git clone do respositório no seguinte path `/home/$USER/Documents/github/`
2. O git clone desse repositório deve estar dentro do path `/home/$USER/Documents/bitbucket/`
2. Na sua máquina local, desative o rabbitMQ, você usará o rabbitMQ do seu container! `sudo service rabbitmq-server stop`. O container não conseguirá ser instanciado se o rabbitMQ estiver UP na sua máquina!
3. Faça o teste do docker para saber se a instação ocorreu com sucesso: `docker run hello-world`
4. Tudo pronto! Neste diretório rode `./build.sh` no seu terminal! Esse processo, se realizado da primeira vez levará um tempo, é o tempo do Docker Daemon importar a imagem .tar desse repositório e instanciar o container.
5. Aguarde alguns instantes antes de acessar `http://localhost:8000`. O script de inicialicação irá configurar o rabbitMQ pela primeira vez (em torno de uns 3~5 minutos).
6. Perfeito! Verifique se a aplicação está de pé na porta 8000 e na porta 15672!
8. Sete todas as filas (como o rabbitMQ foi inicializado pela primeira vez dentro do container, ele está limpo! Mas a auth é a mesma test:test)
9. Para rodar os comandos manage.py basta abrir um terminal e executar o script `./exec.sh`! O seu terminal abrirá um shell dentro do container. Daí é só cd para a pasta do internal-apps: `cd /opt/altomarcio/internal-apps`. O repostiório se encontra nesse path
10. PROFIT

### 2. Arquitetura

*ENGLISH*

The image’s main goal is to isolate all the environment of the Internal Apps System, deploying the whole Internal Apps system inside a container. The main advantage of this is that all system services and libraries and, therefore, all changes made inside the container are not reflected outside making it a ‘sandbox’ free for development.

This image was developed to allow Dafiti developers to use their favorite IDE, also to allow GIT-ing outside the image (making it more secure, since SSH keys are not hard-coded anywhere.

![Ach](/images/docker-architecture.png)

The container allows the development of the application outside the container by using a bind mount; to make this happen, the repository must be cloned, on the host, on this path $HOME/Documents/github/internal-apps. This repository will be mounted inside the container on the path /opt/altomarcio/internal-apps. By using a bind mount all the changes made inside the container on the host path will be reflected on the target path (making it possible to code outside the container).

There is also a docker volume mounted on the host to allow Data Persistence, meaning that all changes made inside the container - files (outside the mounted path), services, libraries - will be persisted for other instances of the same container (in case of docker rm or docker kill).

This image uses --network=host, that means that all network interfaces of the host are ‘replicated’ (not really replicated, it works similar to overlapping) to the container. This way you need to make sure that RabbitMQ is not running on your machine (host).

### 3. Problemas

Qualquer tipo de problema sinta-se a vontade para mandar uma mensagem guilherme.junior@dafiti.com.br, MÃS, explore os logs do container `docker logs $(docker ps -q -f name=altomarcio)` antes de tudo. Se o container ainda estiver 'de pé' (verifique através do docker ps)
