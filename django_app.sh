#!/bin/bash

set -e

service rabbitmq-server start
rabbitmq-plugins enable rabbitmq_management
rabbitmqctl add_user test test
rabbitmqctl set_user_tags test administrator
rabbitmqctl set_permissions -p / test ".*" ".*" ".*"

# Really simple stuff
cd /opt/$IA_USER/internal-apps/
exec python manage.py runserver