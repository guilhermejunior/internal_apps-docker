#!/bin/bash

# Simple script to run container for **DEVELOPMENT ONLY**.
# Here all the integration for dpeloyment happens using a bind mount
# that means that you can use your favorite IDE to develop and all changes
# are reflected inside the container!

# The repository should be located inside /home/$USER/Documents/github/internal-apps

# It also means that git committing, creating branches, etc. happens in
# the host and **not inside the container**. If you want to install new libraries
# migrate, etc.:
# 1) you need to access the container using: docker exec -it internal-apps /bin/bash
#    then you can run any commands you want

# Also, the DB is in the host, so you can access it normally using DBeaver or the 
# software of your choice.

set -e

# cd to bitbucket folder repo
cd $HOME/Documents/bitbucket/internal_apps-docker
docker image ls | grep 'internal_apps'

if [[ $? -eq 1 ]]; then
    docker load < internal_apps.tar
fi

# First we create a volume to persiste the state of our container
echo "Creating local volume for state persistance..."
docker volume ls | grep 'django_volume'

if [[ $? -eq 1 ]]; then
    docker volume create django_volume
fi

# Then we create our container
echo "Creating our container (detached mode)..."
# Our user is altomarcio (automatico) because I'm funny and our image is internal_apps
docker run -d --name=altomarcio \
            --mount type=bind,source=$HOME/Documents/github/internal-apps,target=/opt/altomarcio/internal-apps \
            -v django_volume --network=host --user=root \
            internal_apps

echo "Wait a couple of minutes so the application inside the container can start..."
echo "Access localhost:8000 (web-application) and localhost:15672 (rabbitmq-server) =)"