FROM ubuntu:18.04

LABEL MANTAINER="Automation - GFG Brazil"

ARG IA_USER="altomarcio"
ARG IA_UID="1000"
ARG IA_GID="100"

USER root

# First we need to install 
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && \
    apt-get -yq dist-upgrade && \
    apt-get install -yq --no-install-recommends \
    gcc \ 
    clang \
    wget \
    bzip2 \
    ca-certificates \
    sudo \
    python \
    python-pip \
    python-dev \
    libssl-dev \
    libffi-dev \
    libxml2-dev \
    python2.7-dev \
    python-setuptools \
    sqlite \
    erlang \
    libxslt1-dev \
    zlib1g-dev \
    libxml2-dev \
    rabbitmq-server \ 
    libmysqlclient-dev \
    python-mysqldb \
    libcurl4-gnutls-dev \
    librtmp-dev \ 
    build-essential \ 
    libc-dev 

ADD fix-permissions /usr/local/bin/fix-permissions

# Let's add our User
ENV SHELL=/bin/bash \
    IA_USER=$IA_USER \
    IA_UID=$IA_UID \
    IA_GID=$IA_GID

ENV HOME=/home/$IA_USER

RUN groupadd wheel -g 11 && \
    echo "auth required pam_wheel.so use_uid" >> /etc/pam.d/su && \
    useradd -m -s /bin/bash -N -u $IA_UID $IA_USER && \
    chmod g+w /etc/passwd && \
    fix-permissions $HOME && \
    mkdir /tmp/internal-apps/

USER $IA_UID

RUN mkdir /home/$IA_USER/work && \
    fix-permissions /home/$IA_USER

ADD requirements.txt /tmp/internal-apps

RUN pip install --user wheel

RUN pip install --upgrade setuptools --user && \
    pip install -r /tmp/internal-apps/requirements.txt

USER root

ADD django_app.sh /bin/django_app.sh
RUN chmod +x /bin/django_app.sh

# Clean-up
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# EXPOSE 8000 15672
WORKDIR $HOME

ENTRYPOINT [ "/bin/django_app.sh" ]

USER $IA_UID